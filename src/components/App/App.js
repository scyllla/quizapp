import React from 'react';
import Header from '../Header/index'
import Landing from '../Landing/index'
import '../../App.css';
import Footer from '../Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <Landing />
      <Footer />
    </div>
  );
}

export default App;
