import React from 'react'

const Footer = () => {
    return (
        <footer>
            <div className="footer-container">
                <p>Project réalisé par Scyll'AS - 2020</p>
            </div>
        </footer>
    )
}

export default Footer
